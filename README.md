# pruning technique of polar codes



## ReadMe

The project gives a pruning algorithms for polar codes with SC decoding under the BEC and AWGN channels.
The technique can arrive a low latency for construction and decoding processes by pruning the redundant branches of the tree model of polar codes, reducing the running time relative to the conventional methods presented by Arikan. 

