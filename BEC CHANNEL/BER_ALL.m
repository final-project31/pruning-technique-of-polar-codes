%Comparison of BER for all algorithms 
clear
clc
%PARAMETERS
n=10;
N=2^n;
epsilon=0.2:0.05:0.7;
Code_rate=0.5;
Target_E=0.01;
%SIMULATION
simulate_num=100;
error_rate_1=zeros(1,length(epsilon));
error_rate_2=zeros(1,length(epsilon));
error_rate_3=zeros(1,length(epsilon));
error_rate_4=zeros(1,length(epsilon));
error_rate_5=zeros(1,length(epsilon));
error_rate_6=zeros(1,length(epsilon));

for i=1:length(epsilon) 
    ber1=zeros(1,simulate_num);
    ber2=zeros(1,simulate_num);
    ber3=zeros(1,simulate_num);
    ber4=zeros(1,simulate_num);
    ber5=zeros(1,simulate_num);
    ber6=zeros(1,simulate_num);
    for j=1:simulate_num
        %Input signal
        %%% Generate a binary input vector of size 1 x K
        K=floor(Code_rate*N); %The number of information bits per block
        signal=randi([0 1],1,K);
        % display(signal);
        
        %Fully polarization
        Decoded_Signal_1=Fully_Polarization(n,Code_rate,epsilon(i),signal);
        
    %     clearvars -except error_rate1 signal epsilon Code_rate Target_E n tt1 
        %Simplified decoding
        Decoded_Signal_2=Simplified_Polarization(n,Code_rate,epsilon(i),signal);
        
    %     clearvars -except error_rate1 error_rate2 signal epsilon Code_rate Target_E n tt1 tt2
        %Relaxed polarization
        Decoded_Signal_3=Relaxed_Polarization(n,Code_rate,epsilon(i),Target_E,signal);
        
    %     clearvars -except error_rate1 error_rate2 error_rate3 signal epsilon Code_rate Target_E n tt1 tt2 tt3
        %SSCD-Relaxed polarization
        Decoded_Signal_4=SSCD_Relaxed_Polarization(n,Code_rate,epsilon(i),Target_E,signal);
        
    %     clearvars -except error_rate1 error_rate2 error_rate3 error_rate4 signal epsilon Code_rate Target_E n tt1 tt2 tt3 tt4
        %Further-Relaxed Polarization
        Decoded_Signal_5=Further_Relaxed_Polarization(n,Code_rate,epsilon(i),signal);

        %Further SSCD-Relaxed Polarization
        Decoded_Signal_6=Further_SSCD_Relaxed_Polarization(n,Code_rate,epsilon(i),signal);
    
        %Check bit error rate
        ber1(j)=length(find(Decoded_Signal_1~=signal))/length(signal);
        ber2(j)=length(find(Decoded_Signal_2~=signal))/length(signal);
        ber3(j)=length(find(Decoded_Signal_3~=signal))/length(signal);
        ber4(j)=length(find(Decoded_Signal_4~=signal))/length(signal);
        ber5(j)=length(find(Decoded_Signal_5~=signal))/length(signal);
        ber6(j)=length(find(Decoded_Signal_6~=signal))/length(signal);
    end
    
    %Results
    error_rate_1(i)=mean(ber1);
    error_rate_2(i)=mean(ber2);
    error_rate_3(i)=mean(ber3);
    error_rate_4(i)=mean(ber4);
    error_rate_5(i)=mean(ber5);
    error_rate_6(i)=mean(ber6);
%     display(error_rate_1);
%     display(error_rate_2);
%     display(error_rate_3);
%     display(error_rate_4);
%     display(error_rate_5);
end
figure
plot(epsilon,error_rate_1,'r');
hold on
plot(epsilon,error_rate_2,'m--*');
hold on
plot(epsilon,error_rate_3,'b--x');
hold on
plot(epsilon,error_rate_4,'b-x','LineWidth',1.2);
hold on
plot(epsilon,error_rate_5,'k--s');
hold on
plot(epsilon,error_rate_6,'k-s','LineWidth',1.2);
title("Bit error rate with different algorithms");
xlabel('Erasure probability');
ylabel('BER');
legend("Fully","Simplified","Relaxed","SSCD-Relaxed","Further-Relaxed","Further-SSCD-Relaxed",'Location','NorthWest');
saveas(gcf,'BER','png');
