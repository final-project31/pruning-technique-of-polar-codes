%Further SSCD-Relaxed polarization vs Further Relaxed polarization(without sscd)
%Complexity reduction ratio compared with full polarization
close all
clear
clc
%PARAMETERS
n=10;
N=2^n;
epsilon=0.6;
Code_rate=0.3:0.1:0.8;
%SIMULATION
simulate_num=100;


% for nn=1:length(n)
    tt1=zeros(1,simulate_num);
    tt2=zeros(1,simulate_num);
    tt3=zeros(1,simulate_num);
    CR1=zeros(1,length(Code_rate));
    CR2=zeros(1,length(Code_rate));
    for i=1:length(Code_rate) 
    
        for j=1:simulate_num
            %Input signal
            %%% Generate a binary input vector of size 1 x K
            K=floor(Code_rate(i)*N); %The number of information bits per block
            signal=randi([0 1],1,K);
            % display(signal);
            
            %Fully polarization
            t1=clock;
            Decoded_Signal_1=Fully_Polarization(n,Code_rate(i),epsilon,signal);
            t2=clock;
            tt1(j)=etime(t2,t1);
    
            %Further SSCD-Relaxed Polarization
            t1=clock;
            Decoded_Signal_2=Further_Relaxed_Polarization(n,Code_rate(i),epsilon,signal);
            t2=clock;
            tt2(j)=etime(t2,t1);

            %Further SSCD-Relaxed Polarization
            t1=clock;
            Decoded_Signal_3=Further_SSCD_Relaxed_Polarization(n,Code_rate(i),epsilon,signal);
            t2=clock;
            tt3(j)=etime(t2,t1);
            
        
        end
        time1=mean(tt1);
        time2=mean(tt2);
        time3=mean(tt3);
        %Complexity reduction
        CR1(i)=(time1-time2)/time1*100;
        CR2(i)=(time1-time3)/time1*100;
    end
    figure
    plot(Code_rate,CR1);
    hold on
    plot(Code_rate,CR2);
    title("Latency reduction for further-relaxed polarization");
    xlabel('Code rate R');
    ylabel('Latency reduction ratio %');
    legend("Further-Relaxed","Further-SSCD-Relaxed");
    saveas(gcf,'Fur_withsscd_withoutsscd','png');
% end

