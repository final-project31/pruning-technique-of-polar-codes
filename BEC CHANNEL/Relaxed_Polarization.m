%%%%%%%%%%%%%%%%%%%%%% Relaxed polarization %%%%%%%%%%%%%%%%%%%%%%
% Partial nodes will be encoded and decoded only when they satisfy the condition
% The child nodes which are good enough or bad enough do not need to be decoded
% We need to find a threshold to determine whether a node will be polarized furtherly
%                              ---------                   ---------                                          ---------    
% input->node classification->| Encoder |->encoded_input->| Channel |->received_output->| Decoder |->decoded_output
%                              ---------                   ---------                                          ---------
% tic
%Parameters
% n=10;
% N=2^n;%block length
% R=0.7;%code length
% epsilon=0.2;%erasure probability
% Target_E=0.01;
% simulate_num=50;
function Decoded_Signal=Relaxed_Polarization(n,Code_rate,epsilon,Target_E,signal)
    %Block length
    N=2^n;
    R=Code_rate;
%     ERR=0;
%     for t=1:simulate_num
        %%% Calculate the realibility of each channel and choose the information set
        [Info_index,Froz_index,Sorted_nodes]=BP_Relaxed(N,R,epsilon,Target_E);
        % display(Info_index);
%         %%% Generate a binary input vector of size 1 x K
%         K=floor(R*N); %The number of information bits per block
%         signal=randi([0 1],1,K);
%         % display(signal);
        %%% Encode the input vector
        Encoded_Signal=Encoding(N,signal,Info_index,Froz_index);
        % display(Encoded_Signal);
        %%% Simulate the channel(BEC)
        Observed_Signal=BEC_channel(Encoded_Signal,epsilon);
        % display(Observed_Signal);
        %%% Decode the received vector
        [Decoded_Signal,~,~]=Relaxed_Decoding(Observed_Signal,Info_index,Froz_index,Sorted_nodes);
        % display(Decoded_Signal);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         % Check if the transmission was successful
%         if(isequal(Decoded_Signal,signal))
% %             fprintf('Transmission: success\n')
%         else
%             ERR=ERR+1;
% %             fprintf('Transmission: bad luck\n')
%         end
%     end
%     Error_rate=ERR/simulate_num;
% display(error_rate);
% toc

end