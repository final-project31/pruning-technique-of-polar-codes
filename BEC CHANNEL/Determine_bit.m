function Bit_tree=Determine_bit(node_index,LRs,Bit_tree)
%Determine the bit according to the LRs
    %Block length
    N=size(LRs,1);
    n=log2(N);
    %Current layer of this node (the root node is at the 1st layer)
    i_layer=node_index(1);
    %The order of the node at current layer
    i_node=node_index(2);
    %Layer of the current node and its parent node
    n_node=n+2-i_layer;
    N_node=2^(n_node-1);
    
    %对此节点的每一个值求比特
    for i=1:N_node
        %If the bit value has existed, return (for the frozen bits)
        if ~isnan(Bit_tree((i_node-1)*N_node+1:i_node*N_node,n_node))
            return
        else
            if(LRs((i_node-1)*N_node+i,n_node)==0)
                Bit_tree((i_node-1)*N_node+i,n_node)=1;
            elseif(LRs((i_node-1)*N_node+i,n_node)==Inf)
                Bit_tree((i_node-1)*N_node+i,n_node)=0;
            elseif(LRs((i_node-1)*N_node+i,n_node)==1)
                Bit_tree((i_node-1)*N_node+i,n_node)=nan;
            else
%                 display(LRs((i_node-1)*N_node+i,n_node));
%                 error('Unexpected likelihood ratio') 
                return
            end
        end
    end
end