function [Info_index,Froz_index,CR]=BP(N,R,epsilon)
%%%%%%%%%%%%%%%%%%%%%%%
% Compute the bhattacharyya parameters for the synthetic channels obtained from BEC channels
% This will allow to choose which bits should be frozen
% To ontain the better performance, here does not use the recursive struction
%%%%%%%%%%%%%%%%%%%%%%%
% N is the block length 
% R is the code rate 
% epsilon is the ensured probability
%%%%%%%%%%%%%%%%%%%%%%%
    %Check if N is valid
    [f,e]=log2(N);
    if f~=0.5
        error('N was not a power of 2!');
    end
    %Calculate the length of information and frozen bits 
    Len_Info_bits=floor(N*R); % Length of information bits (使用floor意在当N为奇数时冻结位占据数量优势)
    n=e-1; % Bits number=log2(N)
    ZWi=zeros(n+1,N); % I(W)=1-Z(W) 是能传输的最大信息量; Z(w)表示Bhattacharyya parameter (巴氏参数表示最大似然译码的错误概率上界)
    ZWi(1,1)=epsilon;
    %Complexity redunction
    CR=0;
    for i=1:n
        k=2^(i-1);
        for j=1:k
            tmp=ZWi(i,j);
            ZWi(i+1,2*j-1)=2*tmp-tmp^2; %Bad channel
            ZWi(i+1,2*j)=tmp^2; %Good channel
            %complexity computation
            CR=CR+1;
        end 
    end
    [~,index] = sort(ZWi(n+1,:)); % Ascreading order 将最后一层的巴氏参数从小到大排列
    Info_index=sort(index(1:Len_Info_bits)); %The channels with smaller BPs are information channels 前面巴氏参数小的作为信息位
    Froz_index=sort(index(Len_Info_bits+1:end)); %Others are frozen channels 后面的作为冻结位
end