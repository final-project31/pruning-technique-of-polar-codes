function [Decoded_Signal,Bit_tree,LRs]=Relaxed_Decoding(Observed_signal,Info_index,Froz_index,Sorted_tree)
%%%%%%%%%%%%%%%%%%%%%%%
%Function of relaxed decoding
%Decode the signal under the BEC channel 
%Nodes which satisfy the condition(relaxed) will stop furtherly polarize
%%%%%%%%%%%%%%%%%%%%%%%
%Using the LRs
% Observed_signal: the signal need to be decoded
% Info_index: Indecies of information bits
% Froz_index: Indecies of frozen bits
% Sorted_nodes: Show which node is relaxed
%%%%%%%%%%%%%%%%%%%%%%%
    N=length(Observed_signal);%Block length
    % Check if N is valid
    [f,e]=log2(N);
    if f~=0.5
        error('N was not a power of 2!');
    end
    n=e-1;%bit number

    %Solve the tree with all nodes
    all_nodes=get_childnodes([1,1],n+1);
    [row,~]=size(all_nodes);    
    %To decode the information bit, first compute the likelihood ratio of the observed signal
    %For the BEC channel
    LRs=LRs_BEC(Observed_signal);
    %Initial the bit value tree
    Bit_tree=nan(N,n+1);
    Bit_tree(Froz_index,1)=0;
%     display(Sorted_tree);
    for m=2:row %从根节点的第一个子节点开始
        %Calculate the LR of the current node
        current_node=all_nodes(m,:);
%         display(current_node);
        if Sorted_tree(current_node(1),current_node(2))==0 %Bad channel relaxed node
            %First, the LRs do not need to be calculated and the bit tree at this node is set to zero directly
            Bit_tree=Compute_bit(current_node,LRs,Bit_tree,true);
            %Backwads mod2
            all_parent_nodes=get_parentnodes(current_node);
            [row1,~]=size(all_parent_nodes);
            for p=1:row1
                Bit_tree=Compute_bit(all_parent_nodes(p,:),LRs,Bit_tree);
            end
            %Prune all its child nodes by setting the sorted type as nan
            Child_nodes=get_childnodes(current_node,n+1);
            Sorted_tree(sub2ind(size(Sorted_tree),Child_nodes(:,1),Child_nodes(:,2)))=nan;
%             display(Sorted_tree);
%             display(Bit_tree);

         elseif Sorted_tree(current_node(1),current_node(2))==1 %Good channel relaxed node
%             display(current_node);
            LRs=Compute_LRs(current_node,LRs,Bit_tree);
%             display(LRs);
%             display(Sorted_tree);
%             
            %If it's a relaxed node, the polarization also stop and the LRs are good enough to decode the bits
            Bit_tree=Determine_bit(current_node,LRs,Bit_tree);

            %Mod-2 operation gets the bit values of its child nodes until the leaf node
            Bit_tree=Forwards_mod(current_node,Bit_tree);
%             display(Bit_tree);
            %Backwads mod2
            all_parent_nodes=get_parentnodes(current_node);
            [row1,~]=size(all_parent_nodes);
            for p=1:row1
                Bit_tree=Compute_bit(all_parent_nodes(p,:),LRs,Bit_tree);
            end
  
        elseif Sorted_tree(current_node(1),current_node(2))==2%Normal node
            if mod(current_node(2),2)==1 %Left node (calculate by the f operation)
                LRs=Compute_LRs(current_node,LRs,Bit_tree);
                if current_node(1)==n+1
                    Bit_tree=Compute_bit(current_node,LRs,Bit_tree);
                end
            else %Right node (calculate by the g operation with the left node's bit value and parent node's LRs)
                %Use the bit value of the correspond left node
                LRs=Compute_LRs(current_node,LRs,Bit_tree);
%                 display(LRs);
                if current_node(1)==n+1
                    all_parent_nodes=get_parentnodes(current_node);
                    [row1,~]=size(all_parent_nodes);
                    for p=1:row1
                        Bit_tree=Compute_bit(all_parent_nodes(p,:),LRs,Bit_tree);
                    end
                end
            end
%             display(Bit_tree);

        elseif isnan(Sorted_tree(current_node(1),current_node(2)))%Skip the node
            continue
        else
            error("Not defined type of the node");
         end
    end
            
    Decoded_Signal=Bit_tree(Info_index,1)';
end