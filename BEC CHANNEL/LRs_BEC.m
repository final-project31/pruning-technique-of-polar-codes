function LRs=LRs_BEC(Observed_signal)
% Note that only this part is specific to BEC
    %Block length
    N=length(Observed_signal);
    n=log2(N);
    %Initial Parameters
    LRs=zeros(N,n+1);
    %Calculate the LLR for the observed signal at the last column
        for i=1:N
            if(Observed_signal(i)==0)
                LRs(i,n+1)=Inf;
            elseif(Observed_signal(i)==1)
                LRs(i,n+1)=0;
            elseif(isnan(Observed_signal(i))) % Erasure
                LRs(i,n+1)=1;
            else
                error('Unexpected value for observed signal')
            end
    
        end
end