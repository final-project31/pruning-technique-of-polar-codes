function Encoded_Signal=Encoding(N,signal,Info_index,Froz_index)
%%%%%%%%%%%%%%%%%%%%%%%
%Function of encoding
%Using the generate matrix G to get the encoded signal
%%%%%%%%%%%%%%%%%%%%%%%
% N is the block length 
% signal is the information need to be transfered
% Info_index:  Indecies of information bits
% Froz_index: Indecies of frozen bits
%%%%%%%%%%%%%%%%%%%%%%%
    % Check if N is valid
    [f,e]=log2(N);
    if f~=0.5
        error('N was not a power of 2!');
    end  
    % Check if word and indices are valid
    if length(signal)~=length(Info_index)
        error('Signal has different length with the number of the information bits!');
    end 
    %Bits number
    n=e-1;
    %Frozen bit
    frozen=zeros(1,length(Froz_index));
    %Generator matrix
    F=[1 0;1 1];
    G=F; % Initialize the generate matrix
    for i=2:n
        Z=zeros(2^(i-1),2^(i-1));
        G=[G Z;G G];
    end
    %陪集码
    Encoded=signal*G(Info_index,:)+frozen*G(Froz_index,:);
    Encoded_Signal=mod(Encoded,2); %对2取模
end