function LRs=Compute_LRs(node_index,LRs,Bit_tree)
%%%%%%%%%%%%%%%%%%%%%%%
% Function of compute the likelihood ratio of a bit (equivalent to the backwards propagation)
%%%%%%%%%%%%%%%%%%%%%%%
% N is the block length
% current_level is the current layer and the root is the first layer(1), the leaf is the last layer(n+1)
% current_offset is the offset of the current level
% Observed_signal is the recieved signal
% Pre_decoded_bit is the previous bits we have calculated
% bit_index is the index of the bit we need to calculate now
% LRs is the likelihood ratio matrix calculated from previous step
%%%%%%%%%%%%%%%%%%%%%%%     
    %Block length
    N=size(LRs,1);
    n=log2(N);
    %Current layer of this node (the root node is at the 1st layer)
    i_layer=node_index(1);
    %The order of the node at current layer
    i_node=node_index(2);
    %Layer of the current node and its parent node
    n_node=n+2-i_layer;
    N_node=2^(n_node-1);
    n_parent=n_node+1;
    %Code length of its parent node
    N_parent=2^n_node; 
    %Calculate its parent nodes
    L1=LRs(N_parent*ceil(i_node/2)-N_parent+1:N_parent*ceil(i_node/2)-N_node,n_parent);
    L2=LRs(N_parent*ceil(i_node/2)-N_node+1:N_parent*ceil(i_node/2),n_parent);
    %Determine the node is left or right node
    if mod(i_node,2)==1 %Left node
        for j=1:N_node
            % Use table decision for border cases 0/0, Inf/Inf (make diagram to understand)
            if((L1(j)==0&&L2(j)==0)||(isinf(L1(j))&&isinf(L2(j))))
                LRs(N_node*(i_node-1)+j,n_node)=Inf;
            elseif((L1(j)==0&&isinf(L2(j)))||(isinf(L1(j))&&L2(j)==0))
                LRs(N_node*(i_node-1)+j,n_node)=0;
            elseif((L1(j)==1&&isinf(L2(j)))||(isinf(L1(j))&&L2(j)==1))
                LRs(N_node*(i_node-1)+j,n_node)=1;
            else
                LRs(N_node*(i_node-1)+j,n_node)=(L1(j)*L2(j)+1)/(L1(j)+L2(j));
            end
        end   
        
    else %Right node
        for j=1:N_node
            if Bit_tree((i_node-2)*N_node+j,n_node)==0 %该右节点所对应的左节点的比特值 LRs=(L1)^(1-2*ui)*L2
                LRs((i_node-1)*N_node+j,n_node)=L2(j)*L1(j);
            else
                LRs((i_node-1)*N_node+j,n_node)=L2(j)/L1(j);
            end
        end
    end
end