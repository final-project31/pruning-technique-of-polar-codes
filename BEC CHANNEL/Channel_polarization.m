%%%%%%%%% Channel Polarization %%%%%%%%%%%%
%Show the polarized capacity under the BEC channel
%Parameters
%block length
n=10;
N=2^n;
%erasure probility
epsilon=0.5;
%Initialization
IWi=zeros(n+1,N); % I(W)=1-Z(W) 是能传输的最大信息量;
IWi(1,1)=1-epsilon;
for i=1:n
    k=2^(i-1);
    for j=1:k
        tmp=IWi(i,j);
        IWi(i+1,2*j-1)=tmp^2; %Bad channel
        IWi(i+1,2*j)=2*tmp-tmp^2; %Good channel
    end 
end
%Plot
figure
scatter(1:N,IWi(n+1,:),'.');
title('Channel Polarization under BEC');
xlabel('Subchannel Index');
ylabel('Symmetric Capacity');
saveas(gcf,'Channel polarization','png');