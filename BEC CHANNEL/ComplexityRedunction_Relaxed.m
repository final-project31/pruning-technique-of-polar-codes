%Complexity redunction comparison for construction step
%Full polarization vs Relaxed polarization

%Parameters
n=10;
N=2^n;
R=0.5;
epsilon=[10e-4,10e-3,10e-2,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.99,0.999];
Target_E=0.01;
Target_E_Fur=N^(-1/4.9);
CR_ratio=zeros(1,length(epsilon));
CR_ratio_fur=zeros(1,length(epsilon));

%Construction
for i=1:length(epsilon)
    %Full
    [~,~,CR_full]=BP(N,R,epsilon(i));
    %Relaxed
    [~,~,~,CR_relaxed]=BP_Relaxed(N,R,epsilon(i),Target_E);
    CR_ratio(i)=(CR_full-CR_relaxed)/CR_full;
    %Further relaxed
    [~,~,~,CR_further]=BP_Further_Relaxed(N,R,epsilon(i),Target_E_Fur);
    CR_ratio_fur(i)=(CR_full-CR_further)/CR_full;
end

%Plot the complexity reduction ratio
figure
plot(epsilon,CR_ratio,'-*');
axis([0 1 0 1]);
title('Relaxed Polarization Complexity Reduction')
xlabel('Erasure Probability');
ylabel("Complexity redunction ratio");
grid on
saveas(gcf,'Relaxed Polarization Complexity Reduction','png');

figure
plot(epsilon,CR_ratio,'b-*');
hold on 
plot(epsilon,CR_ratio_fur,'r-*');
axis([0 1 0 1]);
title('Polarization Complexity Reduction')
xlabel('Erasure Probability');
ylabel("Complexity redunction ratio");
grid on
legend('Relaxed','Further relaxed');
saveas(gcf,'Polarization Complexity Reduction','png');