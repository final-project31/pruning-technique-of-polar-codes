function Sorted_nodes=Node_Classification(Info_index,n)
%%%%%%%%%%%%%%%%%%%%%%%
%Solve the classification problem for all nodes in the tree expect the leaf nodes(除去叶子层)
%The nodes are classified to three types: Fixed nodes/Mixed nodes/Infomation nodes
%And record the results in a matrix V_mi
%Seem onformation bits at the leaf nodes as the mixed nodes
%%%%%%%%%%%%%%%%%%%%%%%    
    %Initialization
    V_mi=NaN(n+1,2^n);
    %Use the parameter delta to determine the value of matrix
    for i=0:n
        for m=1:2^i
            if i>0&&(V_mi(i,ceil(m/2))==0||V_mi(i,ceil(m/2))==1||isnan(V_mi(i,ceil(m/2))))
                V_mi(i+1,m)=nan;
            else
                delta=numel(find(ceil(Info_index/2^(n-i))==m));
                %Determine the which type the node is 
                if delta==2^(n-i)%Information node
                    V_mi(i+1,m)=1;
                elseif delta==0%Fixed node
                    V_mi(i+1,m)=0;               
                else
                     V_mi(i+1,m)=2;
                end
            end
        end
    end
    Sorted_nodes=V_mi;
end