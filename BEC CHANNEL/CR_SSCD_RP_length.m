%SSCD-Relaxed polarization
%Complexity reduction ratio for different length at different code rate
clear 
close all
clc
%PARAMETERS
n=[8,10];
N=2.^n;
epsilon=0.6;
Code_rate=0.3:0.1:0.8;
Target_E=0.00001;
%SIMULATION
simulate_num=100;


for nn=1:length(n)
    tt1=zeros(1,simulate_num);
    tt2=zeros(1,simulate_num);
    CR=zeros(1,length(Code_rate));

    for i=1:length(Code_rate) 
    
        for j=1:simulate_num
            %Input signal
            %%% Generate a binary input vector of size 1 x K
            K=floor(Code_rate(i)*N(nn)); %The number of information bits per block
            signal=randi([0 1],1,K);
            % display(signal);
            
            %Fully polarization
            t1=clock;
            Decoded_Signal_1=Fully_Polarization(n(nn),Code_rate(i),epsilon,signal);
            t2=clock;
            tt1(j)=etime(t2,t1);
    
            %SSCD-Relaxed polarization
            t1=clock;
            Decoded_Signal_2=SSCD_Relaxed_Polarization(n(nn),Code_rate(i),epsilon,Target_E,signal);
            t2=clock;
            tt2(j)=etime(t2,t1);
            
        
        end
        time1=mean(tt1);
        time2=mean(tt2);
    
        %Complexity reduction
        CR(i)=(time1-time2)/time1*100;

    end
    plot(Code_rate,CR);
    hold on   
end
axis([0.3 0.8 10 60])
title("Latency reduction with different algorithms");
xlabel('Code rate R');
ylabel('Latency reduction ratio %');
for s=1:length(n)
    str{s}=['n=',num2str(n(s))];
end
legend(str);
saveas(gcf,'CR_SSCD_Relaxed','png');