function received_output=BEC_channel(encoded_input,epsilon)
%%%%%%%%%%%%%%%%%%%%%%%
% Implement the BEC channel
% The input is ensured with the probability epsilon
%%%%%%%%%%%%%%%%%%%%%%%
    received_output = encoded_input; 
    received_output(rand(1,length(encoded_input))<epsilon)=nan;
end
