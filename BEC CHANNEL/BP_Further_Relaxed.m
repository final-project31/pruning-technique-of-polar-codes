function [Info_index,Froz_index,Sorted_nodes,CR]=BP_Further_Relaxed(N,R,epsilon,Target_E)
%%%%%%%%%%%%%%%%%%%%%%%
% Compute the bhattacharyya parameters for the synthetic channels obtained from BEC channels
% This will allow to choose which bits should be frozen
% Meanwhile, if the Z satisfies the condition, the node will be relaxed and
% the sorted_nodes will be set as 1
% To ontain the better performance, here does not use the recursive struction
%%%%%%%%%%%%%%%%%%%%%%%
% N is the block length 
% R is the code rate 
% epsilon is the ensured probability
%%%%%%%%%%%%%%%%%%%%%%%
    %Check if N is valid
    [f,e]=log2(N);
    if f~=0.5
        error('N was not a power of 2!');
    end
    %Calculate the length of information and frozen bits 
    Len_Info_bits=floor(N*R); % Length of information bits (使用floor意在当N为奇数时冻结位占据数量优势)
    n=e-1; % Bits number=log2(N)
    ZWi=zeros(n+1,N); % I(W)=1-Z(W) 是能传输的最大信息量; Z(w)表示Bhattacharyya parameter (巴氏参数表示最大似然译码的错误概率上界)
    ZWi(1,1)=epsilon;
    %Threshold 
    Tg=Target_E*2^(-n);
    Tb=1-Tg;
    %Complexity redunction
    CR=0;
    %Sorted_nodes
    Sorted_nodes=nan(n+1,N);
    %If the root node is bad or good enough, return
    if ZWi(1,1)<Tg||ZWi(1,1)>Tb
        fprintf('The channel does not need to be polarized furtherly');
        return
    else
        Sorted_nodes(1,1)=2;
    end

    for i=1:n
        k=2^(i-1);
        for j=1:k
            tmp=ZWi(i,j);
            if Sorted_nodes(i,j)==1||Sorted_nodes(i,j)==0||isnan(Sorted_nodes(i,j))
                ZWi(i+1,2*j-1)=tmp;
                ZWi(i+1,2*j)=tmp;
                Sorted_nodes(i+1,2*j-1)=NaN;
                Sorted_nodes(i+1,2*j)=NaN;
            else
                ZWi(i+1,2*j-1)=2*tmp-tmp^2; %Bad channel
                ZWi(i+1,2*j)=tmp^2; %Good channel
                %complexity computation
                CR=CR+1;
                %Determine whether the node is relaxed
                %Left node
                if ZWi(i+1,2*j-1)<Tg %Good channel
                    Sorted_nodes(i+1,2*j-1)=1;
                elseif ZWi(i+1,2*j-1)>Tb %Bad channel
                    Sorted_nodes(i+1,2*j-1)=0;
                else
                    Sorted_nodes(i+1,2*j-1)=2; %Normal channel
                end
                %Right node
                if ZWi(i+1,2*j)<Tg %Good channel
                    Sorted_nodes(i+1,2*j)=1;
                elseif ZWi(i+1,2*j)>Tb %Bad channel
                    Sorted_nodes(i+1,2*j)=0;
                else
                    Sorted_nodes(i+1,2*j)=2; %Normal channel
                end
            end
        end 
    end
%     Sorted_nodes(n+1,:)=0;
    [~,index] = sort(ZWi(n+1,:)); % Ascreading order 
    Info_index=sort(index(1:Len_Info_bits)); %The channels with smaller BPs are information channels 
    Froz_index=sort(index(Len_Info_bits+1:end)); %Others are frozen channels 
end