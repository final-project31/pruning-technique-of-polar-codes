
%Simulation of the complexity reduction
clear
clc
%PARAMETERS
n=10;
N=2^n;
epsilon=0.6;
Code_rate=0.3:0.1:0.8;
Target_E=0.00001;
%SIMULATION
simulate_num=100;
tt1=zeros(1,simulate_num);
tt2=zeros(1,simulate_num);
tt3=zeros(1,simulate_num);
tt4=zeros(1,simulate_num);
tt5=zeros(1,simulate_num);
tt6=zeros(1,simulate_num);
CR2=zeros(1,length(Code_rate));
CR3=zeros(1,length(Code_rate));
CR4=zeros(1,length(Code_rate));
CR5=zeros(1,length(Code_rate));
CR6=zeros(1,length(Code_rate));

for i=1:length(Code_rate) 

    for j=1:simulate_num
        %Input signal
        %%% Generate a binary input vector of size 1 x K
        K=floor(Code_rate(i)*N); %The number of information bits per block
        signal=randi([0 1],1,K);
        % display(signal);
        
        %Fully polarization
        t1=clock;
        Decoded_Signal_1=Fully_Polarization(n,Code_rate(i),epsilon,signal);
        t2=clock;
        tt1(j)=etime(t2,t1);
        
    %     clearvars -except error_rate1 signal epsilon Code_rate Target_E n tt1 
        %Simplified decoding
        t1=clock;
        Decoded_Signal_2=Simplified_Polarization(n,Code_rate(i),epsilon,signal);
        t2=clock;
        tt2(j)=etime(t2,t1);
        
    %     clearvars -except error_rate1 error_rate2 signal epsilon Code_rate Target_E n tt1 tt2
        %Relaxed polarization
        t1=clock;
        Decoded_Signal_3=Relaxed_Polarization(n,Code_rate(i),epsilon,Target_E,signal);
        t2=clock;
        tt3(j)=etime(t2,t1);
        
    %     clearvars -except error_rate1 error_rate2 error_rate3 signal epsilon Code_rate Target_E n tt1 tt2 tt3
        %SSCD-Relaxed polarization
        t1=clock;
        Decoded_Signal_4=SSCD_Relaxed_Polarization(n,Code_rate(i),epsilon,Target_E,signal);
        t2=clock;
        tt4(j)=etime(t2,t1);
        
    %     clearvars -except error_rate1 error_rate2 error_rate3 error_rate4 signal epsilon Code_rate Target_E n tt1 tt2 tt3 tt4
        %Further SSCD-Relaxed Polarization
        t1=clock;
        Decoded_Signal_5=Further_Relaxed_Polarization(n,Code_rate(i),epsilon,signal);
        t2=clock;
        tt5(j)=etime(t2,t1);

        %Further SSCD-Relaxed Polarization
        t1=clock;
        Decoded_Signal_6=Further_SSCD_Relaxed_Polarization(n,Code_rate(i),epsilon,signal);
        t2=clock;
        tt6(j)=etime(t2,t1);
    
    end
    time1=mean(tt1);
    time2=mean(tt2);
    time3=mean(tt3);
    time4=mean(tt4);
    time5=mean(tt5);
    time6=mean(tt6);

    %Complexity reduction
    CR2(i)=(time1-time2)/time1*100;
    CR3(i)=(time1-time3)/time1*100;
    CR4(i)=(time1-time4)/time1*100;
    CR5(i)=(time1-time5)/time1*100;
    CR6(i)=(time1-time6)/time1*100;
end


% display(mean(tt1));
% display(mean(tt2));
% display(mean(tt3));
% display(mean(tt4));
% display(mean(tt5));

figure
plot(Code_rate,CR2,'r--*');
hold on
plot(Code_rate,CR3,'b--x');
hold on
plot(Code_rate,CR4,'b-x','LineWidth',2);
hold on
plot(Code_rate,CR5,'k--s');
hold on
plot(Code_rate,CR6,'k-s','LineWidth',2);

title("Latency reduction with different algorithms");
xlabel('Code rate R');
ylabel('Latency reduction ratio %');
h=legend("Simplified","Relaxed","SSCD-Relaxed","Further-Relaxed","Further-SSCD-Relaxed",'FontSize',5,'Location','NorthWest');
% legend("Further SSCD Relaxed","Further Relaxed");
saveas(gcf,'CRs_all','png');