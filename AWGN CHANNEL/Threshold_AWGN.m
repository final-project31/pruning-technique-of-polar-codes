function [Tg,Tb] = Threshold_AWGN(N,Code_rate,Target_E)
%%%%%%%%%%%%%%%%%%
%Obtain two thresholds
%Tg is the threshold for the good channel
%Tb is the threshold for the bad channel
%%%%%%%%%%%%%%%%%%
    R=Code_rate;
    Tg=vpa(Target_E/(R*N),3);
    Hb=1-(-Tg*log2(Tg)-(1-Tg)*log2(1-Tg));
    Tb=Tbb(Hb);
    Tb=double(Tb);
    Tg=double(Tg);
end