function Bit_tree=Forwards_mod(node_index,Bit_tree)
%When the parent node's bit values are all known, we can use the mod-2
%operation to calculate the bit values of its child nodes
%According to the generate matrix G, we can get the bits of the leaf nodes directly
    N=size(Bit_tree,1);
    %Bits number
    n=log2(N);
    %Current layer of this node (the root node is at the 1st layer)
    i_layer=node_index(1);
    %The order of the node at current layer
    i_node=node_index(2);
    %Layer of the current node
    n_node=n+2-i_layer;
    %Length of current node
    N_node=2^(n_node-1);
    if i_layer==n+1
        return
    end
    if ~isnan(Bit_tree((i_node-1)*N_node+1:i_node*N_node,1))
        return
    end
    %Generator matrix
    F=[1 0;1 1];
    G=F; % Initialize the generate matrix
    for i=2:n_node-1
        Z=zeros(2^(i-1),2^(i-1));
        G=[G Z;G G];
    end
    Bit_tree((i_node-1)*N_node+1:i_node*N_node,1)=mod((Bit_tree((i_node-1)*N_node+1:i_node*N_node,n_node)'*G)',2);

end