%%%%%%%%%%%%%%%%%%%%%% Further SSCD-Relaxed polarization %%%%%%%%%%%%%%%%%%%%%%
% Partial nodes will be encoded and decoded only when they satisfy the condition
% The child nodes which are good enough or bad enough do not need to be decoded
% We need to find a threshold to determine whether a node will be polarized furtherly
% And meanwhile, we also relaxed the rate-0 and rate-1 nodes
%                              ---------                   ---------                                          ---------    
% input->node classification->| Encoder |->encoded_input->| Channel |->received_output->| Decoder |->decoded_output
% 
% tic
% %Parameters
% n=10;
% N=2^n;%block length
% R=0.7;%code length
% epsilon=0.2;%erasure probability
% Target_E=N^(-1/4.9);
% simulate_num=50;
% ERR=0;
function Decoded_Signal=Further_SSCD_Relaxed_Polarization_AWGN(n,Code_rate,snr,sigma_cc,Tg,Tb,signal)
    %Block length
    N=2^n;
    R=Code_rate;
%     ERR=0;
%     for t=1:simulate_num
%         [Tg,Tb]=Threshold_AWGN(N,R,Target_E);
        %%% Calculate the realibility of each channel and choose the information set
        [Info_index,Froz_index,Sorted_tree,~]=GA_Relaxed(N,R,sigma_cc,Tg,Tb);
        % display(Info_index);
%         %%% Generate a binary input vector of size 1 x K
%         K=floor(R*N); %The number of information bits per block
%         signal=randi([0 1],1,K);
%         % display(signal);
        %%% Encode the input vector
        Encoded_Signal=Encoding(N,signal,Info_index,Froz_index);
        % display(Encoded_Signal);
        %%% Simulate the channel(BEC)
        [~,LLR]=AWGN_channel(Encoded_Signal,R,snr);
        % display(Observed_Signal);
        %%% Decode the received vector
        [Decoded_Signal,~,~]=SSCD_Relaxed_Decoding_AWGN(LLR,Info_index,Froz_index,Sorted_tree);
        % display(Decoded_Signal);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end