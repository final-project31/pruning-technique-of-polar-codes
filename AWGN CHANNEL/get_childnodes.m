function child_nodes=get_childnodes(bit_index,layer)
%%%%%%%%%%%%%%%%%%%%%%%
%For a tree, we calculate the indecies of all child nodes for a given node 
%%%%%%%%%%%%%%%%%%%%%%%
    if(bit_index(1)~=layer)        
        %find the child nodes for a given node(from top to bottom)
        row=bit_index(1);
        col=bit_index(2);
        out_index_left=[row+1,2*col-1]; 
        out_index_right=[row+1,2*col];
        subnodes=cat(1,out_index_left,out_index_right);
        node_1=get_childnodes(subnodes(1,:),layer);
        node_2=get_childnodes(subnodes(2,:),layer);
        child_nodes=cat(1,bit_index,node_1,node_2);
    else
        child_nodes=bit_index;
    end
end
