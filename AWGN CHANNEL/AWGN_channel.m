function [Observed_Signal,LLR] = AWGN_channel(encoded_input,R,snr)
%%%%%%%%%%%%%%%%%%%%%%%
% Implement the AWGN channel with BPSK
% The input is snr and code rate of the channel
%%%%%%%%%%%%%%%%%%%%%%%
    %Block length
    N=length(encoded_input);
    %BPSK
    bpsk=1-2*encoded_input;
    %Noise
    noise=randn(1,N);
    sigma=1/sqrt(2*R)*10^(-snr/20);
%     sigma=10^(-snr/10);
    Observed_Signal=bpsk+noise*sigma;
    %Log likelihood ratio of the observed signal
    LLR=2*Observed_Signal/sigma^2;
end