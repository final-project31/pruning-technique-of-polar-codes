function [Info_index,Froz_index,aver_iter]=GA(N,R,sigma)
%%%%%%%%%%%%%%%%%%%%%%%
% Compute the relibility of each channel using GA for the AWGN channel
% This will allow to choose which bits should be frozen
%%%%%%%%%%%%%%%%%%%%%%%
% N is the block length 
% R is the code rate 
% sigma is the variance
%aver_iter:Number of itervation
%%%%%%%%%%%%%%%%%%%%%%%

    %Check if N is valid
    [f,e]=log2(N);
    if f~=0.5
        error('N was not a power of 2!');
    end
    %Calculate the length of information and frozen bits 
    Len_Info_bits=floor(N*R); % Length of information bits (Using Floor is intended to freeze the number of bits when N is odd)
    n=e-1; % Bits number=log2(N)
    err_prob=zeros(e,N);    
    u=zeros(e,N);
    %Assume the input vector is all zeros
    u(1,1)=2/sigma^2;
    err_prob(1,1)=0.5*erfc(sqrt(u(1,1)/2));
%     a=0;
    num_for_runs=0;
    sum_iter=0;
    for i=1:n
        k=2^(i-1);
        for j=1:k
%             tic
%             a=a+1;
            tmp=u(i,j);
            %upper node
            [u(i+1,2*j-1),num_iter]=phi_inverse(1-(1-phi(tmp))^2);
            err_prob(i+1,2*j-1)=0.5*erfc(sqrt(u(i+1,2*j-1)/2));
            sum_iter=sum_iter+num_iter;
            %lower node
            u(i+1,2*j)=2*tmp;
            err_prob(i+1,2*j)=0.5*erfc(sqrt(u(i+1,2*j)/2));
            num_for_runs=num_for_runs+1;
%             toc
        end
    end
%     a
    aver_iter=sum_iter/num_for_runs;
    %Sort the information and frozen bits
    [~,index]=sort(err_prob(n+1,:)); % Ascreading order
    Info_index=sort(index(1:Len_Info_bits)); %The channels with smaller BPs are information channels 
    Froz_index=sort(index(Len_Info_bits+1:end)); %Others are frozen channels 

    function y = phi(x)
        %phi operation
        if (x>=0)&&(x<=10)
            y=exp(-0.4527*x^0.859 + 0.0218);
        else
            y=sqrt(pi/x)*exp(-x/4)*(1-10/7/x);
        end
    end

    function [x,num_iter]=phi_inverse(y)
    %Inverse phi operation
        if (y<=1.0221)&&(y>=0.0388)
            x=((0.0218-log(y))/0.4527)^(1/0.86);
            num_iter=0;
        else
            x0=0.0388;
            x1=x0-(phi(x0)-y)/derivative_phi(x0);
            delta=abs(x1-x0);
            epsilon=1e-3;
            num_iter=0;
            
            while(delta>=epsilon)
                num_iter=num_iter+1;
                x0=x1;
                x1=x1-(phi(x1)-y)/derivative_phi(x1);
                %当x1过大，放宽epsilon
                if x1>1e2
                    epsilon=10;
                end       
                delta=abs(x1-x0);
            end
            x = x1;
        end
    end


    
    function dx = derivative_phi(x)
        if (x >= 0)&&(x <= 10)
            dx = -0.4527*0.86*x^(-0.14)*phi(x);
        else
            dx = exp(-x/4)*sqrt(pi/x)*(-1/2/x*(1 - 10/7/x) - 1/4*(1 - 10/7/x) + 10/7/x/x);
        end
    end
end