function [Decoded_Signal,Bit_tree]=Fully_Decoding_AWGN(LLR,Info_index,Froz_index)
%%%%%%%%%%%%%%%%%%%%%%%
%Function of decoding
%Decode the signal under the BEC channel
%%%%%%%%%%%%%%%%%%%%%%%
%Using the LRs
% Observed_signal: the signal need to be decoded
% Info_index: Indecies of information bits
% Froz_index: Indecies of frozen bits
%%%%%%%%%%%%%%%%%%%%%%%
    N=length(LLR);%Block length
    % Check if N is valid
    [f,e]=log2(N);
    if f~=0.5
        error('N was not a power of 2!');
    end
    n=e-1;%bit number

    %Solve the tree with all nodes
    all_nodes=get_childnodes([1,1],n+1);
    [row,~]=size(all_nodes);    
    %To decode the information bit, first compute the likelihood ratio of the observed signal
    %For the AWGN channel
    %Initial Parameters
    LRs=zeros(N,n+1);
    LRs(:,n+1)=LLR;
    %Initial the bit value tree
    Bit_tree=nan(N,n+1);
    Bit_tree(Froz_index,1)=0;
    for m=2:row %Start with the first child of the root node
        %Calculate the LR of the current node
        current_node=all_nodes(m,:);
        if mod(current_node(2),2)==1 %Left node (calculate by the f operation)
            LRs=Compute_LRs(current_node,LRs,Bit_tree);
        else %Right node (calculate by the g operation with the left node's bit value and parent node's LRs)
            %Use the bit value of the correspond left node
            left_node=[current_node(1),current_node(2)-1];
            Bit_tree=Compute_bit(left_node,LRs,Bit_tree);
            LRs=Compute_LRs(current_node,LRs,Bit_tree);
            if current_node(1)==n+1
                all_parent_nodes=get_parentnodes(current_node);
                [row1,~]=size(all_parent_nodes);
                for p=1:row1
                    Bit_tree=Compute_bit(all_parent_nodes(p,:),LRs,Bit_tree);
                end
            end
%             Bit_tree
        end
    end
    Decoded_Signal=Bit_tree(Info_index,1)';
end
