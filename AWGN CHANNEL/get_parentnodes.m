function parent_nodes=get_parentnodes(bit_index)
%%%%%%%%%%%%%%%%%%%%%%%
%For a tree, we calculate the indecies of all parent nodes for a given node 
%%%%%%%%%%%%%%%%%%%%%%%
    if(bit_index(1)~=1)        
        %find the parentnodes for a given node(from bottom to top)
        row=bit_index(1);
        col=bit_index(2);
        out_index_parent=[row-1,ceil(col/2)]; 
        node_1=get_parentnodes(out_index_parent);
        parent_nodes=cat(1,bit_index,node_1);
    else
        parent_nodes=bit_index;
    end
end