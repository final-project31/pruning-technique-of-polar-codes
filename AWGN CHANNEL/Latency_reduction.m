%%%%%%%%%%%%%%%%%%%%%%%
%Simulation of the latency reduction
%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
n=11;
N=2^n;
Code_rate=0.3:0.1:0.8;
snr=1.59;
% design_snr=1.59;%C=0.7
Target_E=0.001;
Target_E_f=N^(-1/4.9);
%SIMULATION
simulate_num=100;
tt1=zeros(1,simulate_num);
tt2=zeros(1,simulate_num);
tt3=zeros(1,simulate_num);
tt4=zeros(1,simulate_num);
tt5=zeros(1,simulate_num);
tt6=zeros(1,simulate_num);
CR2=zeros(1,length(Code_rate));
CR3=zeros(1,length(Code_rate));
CR4=zeros(1,length(Code_rate));
CR5=zeros(1,length(Code_rate));
CR6=zeros(1,length(Code_rate));

for i=1:length(Code_rate) 
    %Threshold for relaxed algorithm
    [Tg,Tb]=Threshold_AWGN(N,Code_rate(i),Target_E);
    %Threshold for further relaxed algorithm
    [Tg_f,Tb_f]=Threshold_AWGN(N,Code_rate(i),Target_E_f);

    for j=1:simulate_num
        %Input signal
        %%% Generate a binary input vector of size 1 x K
        K=floor(Code_rate(i)*N); %The number of information bits per block
        signal=randi([0 1],1,K);
        sigma_cc=1/sqrt(2*Code_rate(i))*10^(-snr/20);        
        % display(signal);
        
        %Full polarization
        t1=clock;
        Decoded_Signal_1=Fully_Polarization_AWGN(n,Code_rate(i),snr,sigma_cc,signal);
        t2=clock;
        tt1(j)=etime(t2,t1);
        
        %Simplified decoding
        t1=clock;
        Decoded_Signal_2=Simplified_Polarization_AWGN(n,Code_rate(i),snr,sigma_cc,signal);
        t2=clock;
        tt2(j)=etime(t2,t1);

        %Relaxed polarization
        t1=clock;
        Decoded_Signal_3=Relaxed_Polarization_AWGN(n,Code_rate(i),snr,sigma_cc,Tg,Tb,signal);
        t2=clock;
        tt3(j)=etime(t2,t1);

        %SSCD-Relaxed polarization
        t1=clock;
        Decoded_Signal_4=SSCD_Relaxed_Polarization_AWGN(n,Code_rate(i),snr,sigma_cc,Tg,Tb,signal);
        t2=clock;
        tt4(j)=etime(t2,t1);
        
        %Further SSCD-Relaxed Polarization
        t1=clock;
        Decoded_Signal_5=Further_Relaxed_Polarization_AWGN(n,Code_rate(i),snr,sigma_cc,Tg_f,Tb_f,signal);
        t2=clock;
        tt5(j)=etime(t2,t1);
% 
        %Further SSCD-Relaxed Polarization
        t1=clock;
        Decoded_Signal_6=Further_SSCD_Relaxed_Polarization_AWGN(n,Code_rate(i),snr,sigma_cc,Tg_f,Tb_f,signal);
        t2=clock;
        tt6(j)=etime(t2,t1);

    end

    time1=mean(tt1);
    time2=mean(tt2);
    time3=mean(tt3);
    time4=mean(tt4);
    time5=mean(tt5);
    time6=mean(tt6);

    %Complexity reduction
    CR2(i)=(time1-time2)/time1*100;
    CR3(i)=(time1-time3)/time1*100;
    CR4(i)=(time1-time4)/time1*100;
    CR5(i)=(time1-time5)/time1*100;
    CR6(i)=(time1-time6)/time1*100;
end

figure
plot(Code_rate,CR2,'r--*');
hold on
plot(Code_rate,CR3,'b--x');
hold on
plot(Code_rate,CR4,'b-x','LineWidth',1.5);
hold on
plot(Code_rate,CR5,'k--s');
hold on
plot(Code_rate,CR6,'k-s','LineWidth',1.5);
title("Latency reduction with different algorithms under AWGN");
xlabel('Code rate R');
ylabel('Latency reduction ratio %');
legend("Simplified","Relaxed","SSCD-Relaxed","Further-Relaxed","Further-SSCD-Relaxed",'Location','northwest','FontSize',5);
saveas(gcf,'CRs_all_AWGN','png');

