function Bit_tree=Compute_bit(node_index,LRs,Bit_tree,isfroz)
%Determine the value of the node with the given LRs
%Input
%node_index:The index of the node
%LRs:likelihood ratio
%Bit_tree:bit information of other nodes
%isfroz:determine whether the node is frozed or not
%Output
%Bit_tree:give the bit value of current node
    %Block length
    N=size(LRs,1);
    n=log2(N);
    %Current layer of this node (the root node is at the 1st layer)
    i_layer=node_index(1);
    %The order of the node at current layer
    i_node=node_index(2);
    %Layer of the current node and its parent node
    n_node=n+2-i_layer;
    N_node=2^(n_node-1);
    %Child nodes
    n_child=n_node-1;
    %Code length of its parent node
    N_child=2^(n_child-1);
    %If we don not input the bool parameter isforz, the default value is false
    if (nargin<4)
        isfroz=false;
    end
    
    %If the node already has the value, return (all index in the node have values)
    if ~isnan(Bit_tree((i_node-1)*N_node+1:i_node*N_node,n_node))
        return
    end

    %For the rate-0 node, set the correspond bit tree as 0
    if isfroz
        Bit_tree((i_node-1)*N_node+1:i_node*N_node,n_node)=0;
        return
    end

    %Compute the bit value for all nodes
    if n_node==1%Leaf layer
        Bit_tree=Determine_bit(node_index,LRs,Bit_tree);
    else %Intermediate nodes, mod2 operation        
        %The child nodes bit values of the current node
        u_odd=Bit_tree(i_node*N_node-2*N_child+1:i_node*N_node-N_child,n_child);
        u_even=Bit_tree(i_node*N_node-N_child+1:i_node*N_node,n_child);
        %If there is a node of unknown bit value directly return
        if (isnan(any(u_odd))||isnan(any(u_even)))
            return
        else
            u_upper=mod(u_odd+u_even,2);
            u_lower=u_even;
            Bit_tree((i_node-1)*N_node+1:i_node*N_node,n_node)=cat(1,u_upper,u_lower);
        end
    end
    
end