%%%%%%%%%%%%%%%%%%%%%%%
%Comparison for two knids of thresholds
%SSCD-Relaxed vs Further SSCD-Realxed 
%%%%%%%%%%%%%%%%%%%%%%%

format long
n=[6,8,10,12,14];
N=2.^n;
Code_rate=0.6;
Tg=zeros(1,length(n));
Tg1=zeros(1,length(n));
Tg_f=zeros(1,length(n));

for i=1:length(n)
    %SSCD-Relaxed
    Target_E=0.1;
    Target_E1=0.01;
    %Further SSCD-Realxed 
    Target_E_f=N(i)^(-1/4.9);
    %Threshold for relaxed algorithm
    [Tg(i),~]=Threshold_AWGN(N(i),Code_rate,Target_E);
    [Tg1(i),~]=Threshold_AWGN(N(i),Code_rate,Target_E1);
    %Threshold for further relaxed algorithm
    [Tg_f(i),~]=Threshold_AWGN(N(i),Code_rate,Target_E_f);
end
fprintf("Thresholds for different ways of selection\n")
fprintf('----------SSCD-Relaxed with target FER 0.1----------');
display(Tg);
fprintf('----------SSCD-Relaxed with target FER 0.01----------');
display(Tg1);
fprintf('----------Further SCD-Relaxed----------');
display(Tg_f);