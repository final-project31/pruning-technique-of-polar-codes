%%%%%%%%%%%%%%%%%%%%%% Simplified decoding AWGN %%%%%%%%%%%%%%%%%%%%%%
% All nodes should be encoded and partial nodes will be decoded
% The child nodes of the Rate-1 and Rate-0 nodes do not need to be decoded
%         ---------                   ---------                                          ---------    
% input->| Encoder |->encoded_input->| Channel |->received_output->node classification->| Decoder |->decoded_output
%         ---------                   ---------                                          ---------
%Parameters
% tic
% n=10;
% N=2^n;%block length
% R=0.7;%code length
% epsilon=0.2;%erasure probability
% simulate_num=50;
% ERR=0;
function Decoded_Signal=Simplified_Polarization_AWGN(n,Code_rate,snr,sigma_cc,signal)
    %Block length
    N=2^n;
    R=Code_rate;
%     ERR=0;
%     for t=1:simulate_num
        %%% Calculate the realibility of each channel and choose the information set
        [Info_index,Froz_index,~]=GA(N,R,sigma_cc);
        % display(Info_index);
%         %%% Generate a binary input vector of size 1 x K
%         K=floor(R*N); %The number of information bits per block
%         signal=randi([0 1],1,K);
%         % display(signal);
        %%% Encode the input vector
        Encoded_Signal=Encoding(N,signal,Info_index,Froz_index);
        % display(Encoded_Signal);
        %%% Simulate the channel(BEC)
        [~,LLR]=AWGN_channel(Encoded_Signal,R,snr);
        % display(Observed_Signal);
        %%% Decode the received vector
        [Decoded_Signal,~,~]=Simplified_Decoding_AWGN(LLR,Info_index,Froz_index);
        % display(Decoded_Signal);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end