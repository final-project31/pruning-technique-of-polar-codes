%%%%%%%%%%%%%%%%%%
%Error performance
%Frame error probaility for all algorithms
%%%%%%%%%%%%%%%%%%
%Main
clear
clc
%PARAMETERS
n=10;
N=2^n;
snr=0:0.5:4;
design_snr=1.82;%N=2024 R=0.5%1.64;
Code_rate=0.7;
Target_E=0.001;
Target_E_f=N^(-1/4.9);
%SIMULATION
simulate_num=100;
error_rate_1=zeros(1,length(snr));
error_rate_2=zeros(1,length(snr));
error_rate_3=zeros(1,length(snr));
error_rate_4=zeros(1,length(snr));
error_rate_5=zeros(1,length(snr));
error_rate_6=zeros(1,length(snr));

%Threshold for relaxed algorithm
[Tg,Tb]=Threshold_AWGN(N,Code_rate,Target_E);
%Threshold for further relaxed algorithm
[Tg_f,Tb_f]=Threshold_AWGN(N,Code_rate,Target_E_f);

for i=1:length(snr) 
    
    ERR_1=0;
    ERR_2=0;
    ERR_3=0;
    ERR_4=0;
    ERR_5=0;
    ERR_6=0;
    for j=1:simulate_num
        %Input signal
        %%% Generate a binary input vector of size 1 x K
        K=floor(Code_rate*N); %The number of information bits per block
        signal=randi([0 1],1,K);
        sigma_cc=1/sqrt(2*Code_rate)*10^(-design_snr/20); 
        
        %Fully polarization
        Decoded_Signal_1=Fully_Polarization_AWGN(n,Code_rate,snr(i),sigma_cc,signal);
        
    %     clearvars -except error_rate1 signal epsilon Code_rate Target_E n tt1 
        %Simplified decoding
        Decoded_Signal_2=Simplified_Polarization_AWGN(n,Code_rate,snr(i),sigma_cc,signal);
        
    %     clearvars -except error_rate1 error_rate2 signal epsilon Code_rate Target_E n tt1 tt2
        %Relaxed polarization
        Decoded_Signal_3=Relaxed_Polarization_AWGN(n,Code_rate,snr(i),sigma_cc,Tg,Tb,signal);
        
    %     clearvars -except error_rate1 error_rate2 error_rate3 signal epsilon Code_rate Target_E n tt1 tt2 tt3
        %SSCD-Relaxed polarization
        Decoded_Signal_4=SSCD_Relaxed_Polarization_AWGN(n,Code_rate,snr(i),sigma_cc,Tg,Tb,signal);
        
    %     clearvars -except error_rate1 error_rate2 error_rate3 error_rate4 signal epsilon Code_rate Target_E n tt1 tt2 tt3 tt4
        %Further-Relaxed Polarization
        Decoded_Signal_5=Further_Relaxed_Polarization_AWGN(n,Code_rate,snr(i),sigma_cc,Tg_f,Tb_f,signal);

        %Further SSCD-Relaxed Polarization
        Decoded_Signal_6=Further_SSCD_Relaxed_Polarization_AWGN(n,Code_rate,snr(i),sigma_cc,Tg_f,Tb_f,signal);
    
        %Check
        if(~isequal(Decoded_Signal_1,signal))
            ERR_1=ERR_1+1;
        end
        if(~isequal(Decoded_Signal_2,signal))
            ERR_2=ERR_2+1;
        end
        if(~isequal(Decoded_Signal_3,signal))
            ERR_3=ERR_3+1;
        end
        if(~isequal(Decoded_Signal_4,signal))
            ERR_4=ERR_4+1;
        end
        if(~isequal(Decoded_Signal_5,signal))
%             fprintf("error");
            ERR_5=ERR_5+1;
        end
        if(~isequal(Decoded_Signal_6,signal))
            ERR_6=ERR_6+1;
        end
    end
    
    %Results
    error_rate_1(i)=ERR_1/simulate_num;
    error_rate_2(i)=ERR_2/simulate_num;
    error_rate_3(i)=ERR_3/simulate_num;
    error_rate_4(i)=ERR_4/simulate_num;
    error_rate_5(i)=ERR_5/simulate_num;
    error_rate_6(i)=ERR_6/simulate_num;

end

figure
semilogy(snr,error_rate_1,'r');
hold on
semilogy(snr,error_rate_2,'--m*');
hold on
semilogy(snr,error_rate_3,'--bx');
hold on
semilogy(snr,error_rate_4,'-bx','LineWidth',1.5);
hold on
semilogy(snr,error_rate_5,'--ks');
hold on
semilogy(snr,error_rate_6,'-ks','LineWidth',1.5);
grid on
title("Block error rate with different algorithms");
xlabel('snr');
ylabel('FER');
% xlim([10e-5,10e-0]);
% set(gca,'ytick',[10e-5,10e-4,10e-3,10e-2,10e-1,10e-0]);
legend("Fully","Simplified","Relaxed","SSCD-Relaxed","Further-Relaxed","Further-SSCD-Relaxed",'Location','southwest','FontSize',5);
saveas(gcf,'FER_all_AWGN','png');
% legend("Fully","Simplified","Relaxed","SSCD-Relaxed");
