function Tb = Tbb(y)
%%%%%%%%%%%%%%
%Derive the bad threshold according to the inverse equation
%entropy h=-plogp-(1-p)log(1-p)
%%%%%%%%%%%%%%
    syms x;
    eq=(-x*log2(x)-(1-x)*log2(1-x))==y;
    Tb=vpasolve(eq,x);
    Tb=vpa(Tb,3); 
end